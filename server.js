const express = require('express');
const cors = require('cors');

const app = express();
const router = new express.Router();

app.use(cors());

app.use(express.json());

const basepath = '/apixcloud/clients/v1';

const clients = [{
    name: 'John Doe',
},
{
    name: 'Jane Doe',
}];

router.get('/', (req, res) => {
    res.status(200).json({
        code: 200,
        msg: 'Success',
        result: {
            clients,
        },
    })
});

router.post('/', (req, res) => {
    const { client } = req.body;
    res.status(201).json({
        code: 201,
        msg: 'Success',
        result: {
            client,
            id: 1,
        },
    })
});

router.put('/:id', (req, res) => {
    const { id } = req.params;
    const { client } = req.body;
    res.status(200).json({
        code: 200,
        msg: 'Success',
        result: {
            client,
            id,
        },
    })
});


router.delete('/:id', (req, res) => {
    const { id } = req.params;
    res.status(204).json({
        code: 204,
        msg: 'Success',
        result: {
            id,
        },
    })
});


router.get('/health', (req, res) => {
   res.status(200).send({
        code: 200,
        msg: 'Success',
   });
});

app.use(basepath, router);


const port = process.env.PORT || 4000

app.listen(port, () => {
    console.log(`Server running in ${port}`);
});

