FROM node:14.18.1-alpine

WORKDIR /usr

COPY package.json yarn.lock ./

RUN npm i

COPY . .

EXPOSE 4000

ENV PORT 4000

CMD ["npm", "start"]